<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\IndoregionController;
use App\Http\Controllers\QueryController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::middleware(['auth'])->group(function(){
    Route::redirect('/', '/articles');
    Route::get('/query', [QueryController::class, 'index'])->name('query.index');
    Route::get('/table', [QueryController::class, 'table'])->name('table');
    Route::get('/dependent', [IndoregionController::class, 'region'])->name('dependent');
    Route::post('/regencies', [IndoregionController::class, 'getRegencies'])->name('regencies');
    Route::post('/districts', [IndoregionController::class, 'getDistricts'])->name('districts');
    Route::post('/villages', [IndoregionController::class, 'getVillages'])->name('villages');
    Route::controller(CommentController::class)->group(function () {
        Route::post("comments/{article}", "store")->name("comments.store");
        Route::delete("comments/{comment}", "destroy")->name("comments.delete");
    });
    Route::resource('articles', ArticleController::class);

    Route::post("/logout", function () {
        Auth::logout();
        return redirect()->route("login");
    })->name("logout");
});

// login
Route::middleware('guest')->group(function () {
    Route::get('/register', [AuthController::class , 'formRegister'])->name('register');
    Route::post('/register', [AuthController::class , 'formRegisterProcess']);
    Route::get('/login', [AuthController::class , 'formLogin'])->name('login');
    Route::post('/login', [AuthController::class , 'formLoginProcess']);
});

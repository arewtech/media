$(document).ready(function () {
    $("#table").DataTable({
        ordering: true,
        serverSide: true,
        processing: true,
        ajax: {
            url: $("#table-url").val(),
            data: function (d) {
                // d.name = $('#name').val();
                // d.username = $('#username').val();
                // d.email = $('#email').val();
                // d.role = $("#role").val();
            },
        },
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                searchable: false,
                orderable: false,
                width: "5%",
            },
            {
                data: "name",
                name: "name",
                searchable: true,
                orderable: true,
            },
            // {
            //     data: "username",
            //     name: "username",
            //     searchable: true,
            //     orderable: true,
            // },
            {
                data: "email",
                name: "email",
                searchable: true,
                orderable: true,
            },
            // {
            //     data: "role.name",
            //     name: "role.name",
            //     searchable: true,
            //     orderable: true,
            // },
            {
                data: "action",
                name: "action",
                searchable: false,
                orderable: false,
                width: "10%",
            },
        ],
    });
});

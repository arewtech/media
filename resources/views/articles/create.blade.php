@extends('layouts.app')
@section('content')
    <!-- Content -->
    <section class="p-4">
        <div class="container">
            <h4 class="text-center font-lora">
                <a class="text-decoration-none text-dark" href="{{ route('articles.index') }}">
                    <svg class="me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                    </svg>
                </a>
                Create Article
            </h4>
            <div class="mt-4 gap-4 d-flex justify-content-center">
                <div class="card col-4">
                    <div class="card-body">
                        <form action="{{ route('articles.store') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Title</label>
                                <input type="text" name="title" placeholder="Title" class="form-control"
                                    id="name">
                            </div>
                            <div class="mb-3">
                                <label for="name" class="form-label">Author</label>
                                <input type="text" name="author" placeholder="Name" value="{{ auth()->user()->name }}"
                                    class="form-control" id="name">
                            </div>
                            <div class="mb-3">
                                <label for="desa" class="form-label">Categories</label>
                                <select class="form-select" name="category_id" id="desa"
                                    aria-label="Default select example">
                                    <option selected>-- Pilih Category --</option>
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="name" class="form-label">Desc</label>
                                <textarea name="description" id="desc" cols="20" rows="10" class="form-control" placeholder="Desc..."></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

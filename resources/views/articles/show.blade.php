@extends('layouts.app')
@section('content')
    <!-- Content -->
    <section class="p-4">
        <div class="container">
            <h4 class="text-center font-lora">
                <a class="text-decoration-none text-dark" href="{{ route('articles.index') }}">
                    <svg class="me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                    </svg>
                </a>
                Detail {{ $article->title }}
            </h4>
            <div class="mt-4 gap-4 d-flex justify-content-center">
                <div style="border: none" class="card w-50">
                    <img src="/foods.jpg" style="height: 330px; object-fit: cover" class="card-img-top shadow-lg"
                        alt="...">
                    <div class="card-body">
                        <span style="font-style: italic" class="form-text d-block text-center text-muted">Created By :
                            <span class="text-capitalize">{{ $article->author }}</span>,
                            {{ $article->created_at->diffForHumans() }}</span>
                        <div class="card-title text-start mt-3">
                            <h5 class="font-lora" style="font-size: 1.5rem">{{ $article->title }}</h5>
                        </div>
                        <p class="card-text">{{ $article->description }}</p>
                        <span class="text-muted card-text">{{ $article->blog_info }}</span>
                        @if ($comments->count())
                            @foreach ($comments as $comment)
                                <div class="mb-4 border-top pt-4 mt-4">
                                    {!! str($comment->body)->markdown() !!}
                                    <small class="text-muted d-flex align-items-center gap-2">
                                        {{ $comment->user->name }} &middot; {{ $comment->created_at->diffForHumans() }}
                                        &middot;
                                        @auth
                                            @if (Auth::user()?->id == $comment->user_id)
                                                <form id="delete-comment-form-{{ $comment->id }}" method="POST"
                                                    action="{{ route('comments.delete', $comment) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a class="text-danger text-decoration-none" href="#"
                                                        onclick="confirmDelete('{{ $comment->id }}');">
                                                        Delete
                                                    </a>
                                                </form>
                                            @endif
                                        @endauth
                                    </small>
                                </div>
                            @endforeach
                        @else
                            <p class="text-muted">Be the first to comment!</p>
                        @endif
                        <hr class="my-3">
                        @auth
                            <!-- Form -->
                            @foreach ($errors->all() as $item)
                                {{ $item }}
                            @endforeach
                            <form class="mb-4" action="{{ route('comments.store', $article) }}" method="POST">
                                @csrf
                                <textarea class="form-control" name="body" id="body"
                                    placeholder="Hi {{ auth()->user()?->name }}. What's on your mind ?"></textarea>
                                <div class="text-end mt-2">
                                    <button type="submit" class="btn btn-primary">Comment</button>
                                </div>
                            </form>
                        @else
                            <a href="{{ route('login') }}">Login</a> to make a comment.
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        function confirmDelete(commentId) {
            var confirmation = confirm('Are you sure you want to delete this comment?');
            if (confirmation) {
                document.getElementById('delete-comment-form-' + commentId).submit();
            }
        }
    </script>
@endsection

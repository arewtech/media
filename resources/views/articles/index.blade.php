@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="p-3 mt-4">
            <h5 class="text-center mb-4">
                OOP (Pemrograman Berorientasi Objek)
            </h5>
            <p>
                <b>Object-Oriented Programming</b>, adalah paradigma pemrograman yang menggunakan konsep objek untuk
                mendesain dan
                mengorganisir kode. Objek dalam konteks ini adalah instansiasi dari kelas, yang merupakan blueprint atau
                template untuk objek. Setiap objek dapat memiliki atribut (variabel) dan metode (fungsi) yang terkait
                dengannya. Konsep utama dalam OOP melibatkan empat pilar:
            </p>
            <p>
                Di bawah ini aku akan memberikan contoh penerapan konsep OOP dalam Laravel dengan menggunakan contoh
                sederhana. Sebagai contoh, kita akan membuat artikel atau blog sederhana dengan beberapa kolom: Title,
                Author, dan Published at, Status dan ada juga Category dari table Categories yang foreign idnya ada di table
                Articles begitupun untuk Author. Kami akan menggunakan Eloquent, ORM (Object-Relational Mapping) di Laravel,
                untuk
                menghubungkan model dengan tabel dalam database.
            </p>
            <span class="fw-bold">Table Artikel</span>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center" scope="col">No</th>
                            <th scope="col">Title</th>
                            <th scope="col">Author</th>
                            <th class="text-center" scope="col">Category</th>
                            <th class="text-center" scope="col">Status</th>
                            <th class="text-center" scope="col">Publish</th>
                            <th class="text-center" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($articles as $item)
                            <tr>
                                <th class="text-center" scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td class="text-center">{{ $item->category->name }}</td>
                                <td class="text-center">
                                    <span
                                        class="badge {{ $item->status == 'pending' ? 'bg-warning' : ($item->status == 'published' ? 'bg-success' : 'bg-secondary') }}">{{ $item->status }}</span>
                                </td>
                                <td class="text-center">{{ $item->published_at->format('d/m/y') }}</td>
                                <td class="text-center">
                                    <a href="{{ route('articles.edit', $item) }}" class="btn btn-sm btn-warning">Edit</a>
                                    <form action="{{ route('articles.destroy', $item) }}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-sm btn-danger"
                                            onclick="return confirm('Yakin ingin menghapus data ini?')">Delete</button>
                                    </form>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">Data Kosong</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

            {{--  penerapan --}}
            <p>
                Kita lanjutkan dalam contoh penerapan inheritance, polimorphism juga menerapkan SOLID principles dalam
                laravel</p>
            <div>
                <h5>Enkapsulasi (Encapsulation):</h5>

                <ul>
                    <li>
                        <b>Deskripsi:</b> Enkapsulasi melibatkan pembungkusan data (variabel) dan metode (fungsi) yang
                        beroperasi
                        pada data
                        ke dalam satu unit yang disebut objek.
                    </li>
                    <li>
                        <b>Mengapa Penting:</b>
                        <br />
                        <b>Keamanan Data:</b> Enkapsulasi membantu melindungi data dengan membatasi akses langsung ke
                        variabel,
                        yang dapat
                        mencegah modifikasi atau manipulasi yang tidak sah.
                    </li>
                    <li>
                        <b>Abstraksi:</b> Menggunakan enkapsulasi memungkinkan pembuat kelas untuk menyembunyikan detail
                        implementasi
                        internal dari pengguna objek. Ini membuat objek dapat digunakan tanpa perlu mengetahui detail
                        bagaimana
                        objek tersebut bekerja di dalamnya.
                    </li>
                </ul>

                <h5>Warisan (Inheritance):</h5>
                <ul>
                    <li>
                        <b>Deskripsi:</b> Warisan memungkinkan suatu kelas untuk mewarisi properti (variabel) dan metode
                        (fungsi)
                        dari
                        kelas lain, yang disebut kelas induk atau superclass.
                    </li>
                    <li>
                        <b>Mengapa Penting:</b>
                        Penggunaan Kembali Kode: Dengan menggunakan warisan, Anda dapat menggunakan dan memperluas
                        fungsionalitas
                        yang sudah ada tanpa menulis ulang kode. Ini mempromosikan penggunaan kembali kode dan membuat
                        pengembangan
                        lebih efisien.
                    </li>
                    <li>
                        Pengelolaan dan Struktur Kode: Warisan membantu dalam pengelolaan dan struktur kode dengan
                        memungkinkan
                        pembagian fungsionalitas ke dalam hierarki kelas yang logis.
                    </li>
                </ul>
                <h5>Polimorfisme (Polymorphism):</h5>

                <ul>
                    <li>
                        <b>Deskripsi:</b> Polimorfisme memungkinkan objek untuk merespons pada metode-metode yang sama
                        dengan cara
                        yang
                        berbeda. Ini dapat dicapai melalui overloading dan overriding.
                    </li>
                    <li>
                        <b>Mengapa Penting:</b>
                        <br />
                        <b>Fleksibilitas:</b> Polimorfisme meningkatkan fleksibilitas dalam desain dan pengembangan. Objek
                        dapat
                        dianggap
                        sebagai objek dari kelas induk, yang memungkinkan kelas anak untuk digunakan secara transparan.
                    </li>
                    <li>
                        <b>Code Readability:</b> Polimorfisme meningkatkan kejelasan kode dengan memungkinkan penggunaan
                        metode
                        yang sama
                        dengan nama yang sama pada berbagai kelas, meskipun perilaku sebenarnya dapat bervariasi.
                    </li>
                </ul>
            </div>

            <p>*Yang Pertama aku akan bahas Enkapsulasi</p>
            <h5>Enkapsulasi:</h5>
            <p>
                Mari kita tambahkan contoh Enkapsulasi melibatkan pembungkusan data (variabel) dan metode
                (fungsi) yang beroperasi pada data ke dalam satu unit yang disebut objek, bisa di lihat pada garis warna
                merah.
            </p>
            <div class="d-flex align-items-center gap-2">
                @foreach ($secArticle as $item)
                    <div class="card" style="width: 18rem;">
                        <img src="/foods.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h6 class="card-title line-clamp">{{ $item->title }}</h6>
                            <p class="card-text text-muted line-clamp-3">
                                {{ $item->description }}
                            </p>
                            <div class="border border-2 border-danger">
                                <small style="font-style: italic">
                                    {{ $item->full_info }}
                                </small>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="mt-4">
                <p>*Yang Kedua aku akan bahas Warisan (Inheritance)</p>
                <h5>Warisan (Inheritance):</h5>
                <p>
                    Deskripsi: Warisan memungkinkan suatu kelas untuk mewarisi properti (variabel) dan metode (fungsi) dari
                    kelas lain, yang disebut kelas induk atau superclass. bisa di lihat pada garis warna biru.
                </p>
                <div class="d-flex align-items-center gap-2">
                    @foreach ($inheArticle as $item)
                        <div class="card" style="width: 18rem;">
                            <img src="/foods.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h6 class="card-title line-clamp">{{ $item->title }}</h6>
                                <p class="card-text text-muted line-clamp-3">
                                    {{ $item->description }}
                                </p>
                                <div class="border border-2 border-primary">
                                    <small style="font-style: italic">
                                        {{ $item->blog_info }}
                                    </small>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="mt-4">
                <p>*Yang Ketiga aku akan bahas Polimorfisme (Polymorphism)</p>
                <h5>Polimorfisme (Polymorphism):</h5>
                <p>
                    Mari saya jelaskan lebih rinci tentang konsep polimorfisme dalam konteks Laravel.

                <ul>
                    <li>
                        Dalam konteks Laravel, polimorfisme sering digunakan untuk mengelola hubungan satu tabel dengan
                        banyak
                        model (entitas) lain. Dengan kata lain, kita dapat menggunakan satu tabel untuk menyimpan hubungan
                        banyak jenis objek tanpa perlu membuat tabel terpisah untuk setiap jenis objek tersebut.
                    </li>
                    <li>
                        Dalam contoh ini, kita ingin menyimpan gambar untuk buku dan mungkin jenis objek lainnya di masa
                        depan,
                        tanpa membuat tabel gambar terpisah untuk setiap jenis objek. Kita dapat menggunakan polimorfisme
                        untuk
                        mencapai hal ini.
                    </li>
                </ul>
                Sekarang kita dapat menyimpan komentar untuk artikel dan posting blog tanpa perlu membuat tabel komentar
                terpisah untuk setiap jenis objek.

                Dengan cara ini, kita menggabungkan konsep Enkapsulasi dengan aksesors dan mutators, Warisan dengan membuat
                kelas turunan, dan Polimorfisme dengan menggunakan hubungan polimorfik untuk mengatasi kebutuhan aplikasi
                dengan cara yang bersifat umum dan mudah diperluas.
                </p>
                <p>
                    Jika ingin melihat contohnya klik salah satu artikel dibawah ini, dan lihat pada bagian komentar.
                    Atau kamu juga bisa langsung berkomentar pada artikel tersebut.
                </p>
                <div class="d-flex align-items-center gap-2">
                    @foreach ($commArticle as $item)
                        <div class="card" style="width: 18rem;">
                            <a href="{{ route('articles.show', $item) }}"><img src="/foods.jpg" class="card-img-top"
                                    alt="..."></a>
                            <div class="card-body">
                                <h6 class="card-title line-clamp">{{ $item->title }}</h6>
                                <p class="card-text text-muted line-clamp-3">
                                    {{ $item->description }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

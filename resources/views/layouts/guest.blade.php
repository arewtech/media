<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.meta')
    @include('includes.style')
</head>

<body class="font-sans antialiased">
    <main>
        @yield('content')
    </main>
</body>

</html>

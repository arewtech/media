<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.meta')
    @include('includes.style')
</head>

<body class="font-sans antialiased">
    @include('includes.navbar')
    <main>
        @yield('content')
    </main>
    {{-- style="transform: translate(50%, 50%); right: 50%;" class="position-absolute bottom-0" --}}
    {{-- <footer>
        <span style="font-size: .9rem" class="text-center d-block mt-5 mb-3 text-muted">© 2023 Food Recipes 🧑🏻‍🍳
            | Created By : <a class="text-decoration-none text-dark font-medium"
                href="https://github.com/arewtech">Maman</a>
        </span>
    </footer> --}}
    @include('includes.script')
</body>

</html>

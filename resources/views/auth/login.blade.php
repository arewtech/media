@extends('layouts.guest')
@section('content')
    <div class="col-sm-4 m-auto mt-4">
        <h3>Login</h3>
        @foreach ($errors->all() as $item)
            {{ $item }}
        @endforeach
        @if (session('message'))
            <div class="alert alert-success">{{ session('message') }}</div>
        @endif
        <div class="card">
            <div class="card-body">
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="email" class="form-label">Email address</label>
                        <input type="email" name="email" class="form-control" id="email"
                            placeholder="name@example.com">
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="password..">
                        <div class="form-text">
                            lupa <a href="#">password</a> dulu, klw lu ga ingat massbro😎
                        </div>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                    <div>
                        Tidak punya akun, <a href="{{ route('register') }}">daftar</a> dulu massbro 😎
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<nav class="navbar navbar-expand-lg navbar-dark bg-dark border-bottom border-body">
    <div class="container">
        <a class="navbar-brand text-white font-lora d-md-none" href="{{ url('/') }}">App</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="d-flex flex-md-row flex-column justify-content-between w-100">
                <a class="navbar-brand text-white font-lora d-none d-md-block" href="{{ url('/') }}">App</a>
                <ul class="navbar-nav mb-2 mb-lg-0 order-sm-last">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white text-capitalize" href="javascript:void(0)"
                            role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ auth()->user()->name }}
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg-end">
                            <li>
                                <a class="dropdown-item" href="{{ route('articles.create') }}">Buat Article</a>
                            </li>
                            <hr class="dropdown-divider">
                            <li>
                                <a class="dropdown-item" href="#">My Article</a>
                            </li>
                            <li>
                                <form action="{{ route('logout') }}" method="post">
                                    @csrf
                                    <a class="dropdown-item"
                                        onclick="event.preventDefault();this.closest('form').submit();"
                                        href="javascript:void(0)">Logout</a>
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('articles*') ? 'active' : '' }}" aria-current="page"
                            href="{{ route('articles.index') }}">OOP</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->routeIs('dependent') ? 'active' : '' }}"
                            href="{{ route('dependent') }}">Dependent Dropdown</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link
                        {{ request()->routeIs('query.index') ? 'active' : '' }}"
                            aria-current="page" href="{{ route('query.index') }}">Optimasi
                            Query</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Charts</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

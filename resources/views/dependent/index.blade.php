@extends('layouts.app')
@section('content')
    <!-- Content -->
    <section class="p-4">
        <div class="container">
            <h4 class="text-center font-lora">
                <a class="text-decoration-none text-dark" href="{{ route('articles.index') }}">
                    <svg class="me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                    </svg>
                </a>
                Indo Region
            </h4>
            <div class="mt-4 gap-4 d-flex justify-content-center">
                <div class="card col-4">
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" placeholder="Name" class="form-control" id="name">
                        </div>
                        {{-- provinsi --}}
                        <div class="mb-3">
                            <label for="provinsi" class="form-label">Provinsi</label>
                            <select class="form-select" id="provinsi" aria-label="Default select example">
                                <option selected>-- Pilih Provinsi --</option>
                                @foreach ($provinces as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- kabupaten --}}
                        <div class="mb-3">
                            <label for="kabupaten" class="form-label">Kabupaten</label>
                            <select class="form-select" id="kabupaten" aria-label="Default select example">
                                {{-- <option selected>-- Pilih Kabupaten --</option> --}}
                            </select>
                        </div>
                        {{-- Kecamatan --}}
                        <div class="mb-3">
                            <label for="kecamatan" class="form-label">Kecamatan</label>
                            <select class="form-select" id="kecamatan" aria-label="Default select example">
                                {{-- <option selected>-- Pilih Kecamatan --</option> --}}
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="desa" class="form-label">Desa</label>
                            <select class="form-select" id="desa" aria-label="Default select example">
                                {{-- <option selected>-- Pilih Desa --</option> --}}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Inisialisasi dropdown kabupaten, kecamatan, dan desa sebagai disabled
            $('#kabupaten, #kecamatan, #desa').prop('disabled', true);

            // Provinsi / province
            $('#provinsi').change(function() {
                let province_id = $('#provinsi').val();

                // Mengosongkan dan menonaktifkan dropdown yang lebih tinggi
                $('#kabupaten, #kecamatan, #desa').prop('disabled', true).html('');

                if (province_id) {
                    $.ajax({
                        url: "{{ route('regencies') }}",
                        method: "POST",
                        data: {
                            province_id: province_id
                        },
                        cache: false,
                        success: function(result) {
                            // Menonaktifkan dropdown kecamatan dan desa
                            $('#kecamatan, #desa').prop('disabled', true);
                            // Menampilkan dan mengisi dropdown kabupaten
                            $('#kabupaten').prop('disabled', false).html(result);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            });

            // Kabupaten / regency
            $('#kabupaten').change(function() {
                let regency_id = $('#kabupaten').val();

                // Mengosongkan dan menonaktifkan dropdown yang lebih tinggi
                $('#kecamatan, #desa').prop('disabled', true).html('');

                if (regency_id) {
                    $.ajax({
                        url: "{{ route('districts') }}",
                        method: "POST",
                        data: {
                            regency_id: regency_id
                        },
                        cache: false,
                        success: function(result) {
                            // Menonaktifkan dropdown desa
                            $('#desa').prop('disabled', true);
                            // Menampilkan dan mengisi dropdown kecamatan
                            $('#kecamatan').prop('disabled', false).html(result);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            });

            // Kecamatan / district
            $('#kecamatan').change(function() {
                let district_id = $('#kecamatan').val();

                // Mengosongkan dan menonaktifkan dropdown desa
                $('#desa').prop('disabled', true).html('');

                if (district_id) {
                    $.ajax({
                        url: "{{ route('villages') }}",
                        method: "POST",
                        data: {
                            district_id: district_id
                        },
                        cache: false,
                        success: function(result) {
                            // Menampilkan dan mengisi dropdown desa
                            $('#desa').prop('disabled', false).html(result);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            });
        });
    </script>
@endsection

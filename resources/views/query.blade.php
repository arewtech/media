@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <h5>Datatables</h5>
        <table class="table table-bordered" id="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    {{-- <th>Username</th> --}}
                    <th>Email</th>
                    {{-- <th>Roles</th> --}}
                    <th>Opsi</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection

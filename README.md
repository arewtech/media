# Media

OOP

![media](/public/design/oop.png)

Enkapsulasi (Encapsulation)

![media](/public/design/enkap.png)

Warisan (Inheritance)

![media](/public/design/warisan.png)

Polimorfisme (Polymorphism)

![media](/public/design/poli-text.png)

Polimorfisme (Polymorphism) | Example

![media](/public/design/poli-comment.png)

Dependent Dropdown

![media](/public/design/indoregion.png)

Dependent Dropdown | Example

![media](/public/design/indoselect.png)

Otomasi Query

-   DataTables cocok untuk digunakan ketika data sudah banyak, bahkan mencapai ratusan ribu atau jutaan, karena memiliki beberapa fitur dan strategi yang mendukung kinerja dan navigasi yang efisien seperti server-side processing, pagination, instant search, dan lain-lain.

![media](/public/design/data-table.png)

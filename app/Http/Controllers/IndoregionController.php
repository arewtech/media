<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndoregionController extends Controller
{
    public function region()
    {
        return view('dependent.index',[
            'provinces' => \App\Models\Province::get(),
            // 'regencies' => \App\Models\Regency::get(),
            // 'districts' => \App\Models\District::get(),
            // 'villages' => \App\Models\Village::get(),
        ]);
    }

    public function getRegencies(Request $request)
    {
        $regencies = \App\Models\Regency::where('province_id', $request->province_id)->get();
        $options = "<option>-- Pilih Kabupaten --</option>";
        foreach ($regencies as $item) {
            $options .= "<option value='$item->id'>$item->name</option>";
        }
        echo $options;
    }

    public function getDistricts(Request $request)
    {
        $districts = \App\Models\District::where('regency_id', $request->regency_id)->get();
        $options = "<option>-- Pilih Kecamatan --</option>";
        foreach ($districts as $item) {
            $options .= "<option value='$item->id'>$item->name</option>";
        }
        echo $options;
    }

    public function getVillages(Request $request)
    {
        $villages = \App\Models\Village::where('district_id', $request->district_id)->get();
        $options = "<option>-- Pilih Desa --</option>";
        foreach ($villages as $item) {
            $options .= "<option value='$item->id'>$item->name</option>";
        }
        echo $options;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('articles.index', [
            'articles' => Article::latest()->paginate(10),
            'secArticle' => Article::latest()->limit(2)->get(),
            'inheArticle' => Article::latest()->limit(2)->get(),
            'commArticle' => Article::latest()->limit(2)->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Article $article)
    {
        return view('articles.create',[
            'categories' => Category::select("id", "name")->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $data['category_id'] = $request->category_id;
        $data['slug'] = str($request->title . '-' . str()->random(5))->slug();
        $data['published_at'] = now();
        Article::create($data);
        return redirect()->route('articles.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $article)
    {
        $comments = Comment::query()
            ->select("user_id", "body", "created_at", "id")
            ->with("user")
            ->whereMorphedTo("commentable", $article)
            // whereMorphedTo() = mencari comment yang memiliki commentable_type = App\Models\Article dan commentable_id = $article->id
            ->get();
        return view('articles.show', [
            'article' => $article->load('category'),
            'comments' => $comments,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Article $article)
    {
        return view('articles.edit', [
            'article' => $article,
            'categories' => Category::select("id", "name")->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Article $article)
    {
        $data = $request->all();
        $data['category_id'] = $request->category_id;
        $data['slug'] = str($request->title . '-' . str()->random(5))->slug();
        $data['status'] = $request->status;
        $article->update($data);
        return redirect()->route('articles.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return back();
    }
}

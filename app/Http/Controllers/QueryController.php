<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class QueryController extends Controller
{
    public function index(Request $request){
        $query = User::query();
        return view('query', compact('query'));
    }

    public function table(){
        $query = User::query();
        return DataTables::of($query)
        // ->editColumn('role.name', function($item){
        //     return $item->role->name;
        // })
        ->addIndexColumn()
        ->addColumn('action', function($item){
            return '
            <div class="d-inline-flex">
                <a href="#" class="btn btn-warning btn-sm me-2">
                    Edit
                </a>
                <form action="#" method="POST" class="d-inline">
                    '.method_field('delete').csrf_field().'
                    <button class="btn btn-danger btn-sm">
                        Delete
                    </button>
                </form>
            </div>
            ';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
}

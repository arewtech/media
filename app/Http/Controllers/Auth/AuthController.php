<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function formLogin()
    {
        return view("auth.login");
    }
    public function formLoginProcess(Request $request)
    {
        $credentials = $request->validate([
            "email" => ["required", "email"],
            "password" => ["required"],
        ]);

        if (Auth::attempt($credentials)) {
            // session()->regenerate() maksudnya = menghapus session yang lama dan membuat session yang baru, untuk menghindari session fixation
            // maksudnya = token yang digunakan untuk mengidentifikasi user yang sedang login
            $request->session()->regenerate();

            return redirect()->intended("/");
        }

        return back()
            ->withErrors([
                "email" => "The provided credentials do not match our records.",
            ])
            ->onlyInput("email");
    }

    public function formRegister(Request $request)
    {
        $credentials = $request->validate([
            "name" => ["required"],
            "email" => ["required", "email", "unique:users"],
            "password" => ["required", "confirmed"],
        ]);
            $user = User::create([
                "name" => $credentials["name"],
                "email" => $credentials["email"],
                "password" => bcrypt($credentials["password"]),
            ]);
            Auth::login($user);
        return redirect()->intended("/");
    }
}

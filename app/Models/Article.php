<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['category', 'user'];
    protected $casts = [
        'published_at' => 'datetime',
    ];

    public function getFullInfoAttribute()
    {
        return $this->title . ' created by ' . $this->author;
    }

    // Metode khusus untuk mendapatkan informasi posting blog
    public function getBlogInfoAttribute()
    {
        return $this->getFullInfoAttribute() . ', Category: ' . $this->category->name;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault([
            'name' => 'Guest Author',
        ]);
        // withDefault() = jika user tidak ditemukan, maka akan mengembalikan nilai default
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        // return $this->hasMany(Comment::class);
        return $this->morphMany(Comment::class, 'commentable');
        // morphMany() = untuk mengambil semua komentar yang berkaitan dengan artikel
        // kenapa morphMany() kenapa tidak hasMany()?
        // karena kita menggunakan polymorphic relationship untuk menghubungkan model Comment dengan model Article dan model User (Commentable)
        // berarti commentable_type dan commentable_id karna kita menggunakan morphMany() kah?
        // ya, karena kita menggunakan morphMany() maka kita harus menambahkan commentable_type dan commentable_id pada model Comment
    }
}

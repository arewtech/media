<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // categories article
        $categories = [
            [
                'name' => 'Laravel',
                'slug' => 'laravel',
            ],
            [
                'name' => 'PHP',
                'slug' => 'php',
            ],
            [
                'name' => 'JavaScript',
                'slug' => 'javascript',
            ],
            [
                'name' => 'Vue.js',
                'slug' => 'vue-js',
            ],
            [
                'name' => 'React.js',
                'slug' => 'react-js',
            ],
            [
                'name' => 'Node.js',
                'slug' => 'node-js',
            ],
            [
                'name' => 'Tailwind CSS',
                'slug' => 'tailwind-css',
            ],
            [
                'name' => 'Bootstrap',
                'slug' => 'bootstrap',
            ],
            [
                'name' => 'CSS',
                'slug' => 'css',
            ],
        ];
        foreach ($categories as $category) {
            \App\Models\Category::create($category);
        }
    }
}

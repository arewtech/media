<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $title = $title = str(str($this->faker->sentence(4))->title())->replace(
            ".",
            ""
        );
        return [
            'user_id' => User::factory(),
            'title' => $title,
            'slug' => str($title . '-' . str()->random(6))->slug(),
            'author' => fake()->unique()->name(),
            'category_id' => rand(1, 6),
            'description' => fake()->sentence(100),
            'published_at' => fake()->dateTimeBetween('-1 year', 'now'),
            'status' => fake()->randomElement(['pending', 'published', 'draft']),
            "created_at" => ($created_at = now()->subDays(rand(1, 100))),
            "updated_at" => $created_at,
        ];
    }
}
